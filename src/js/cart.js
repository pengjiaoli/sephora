$(window).load(function(){
    $(".content-middle ul").append('<li><a href="">丝芙兰正品承诺</a></li>');
    var tempIndex=0;
    const autoPlay=()=>{
        tempIndex++;
        if(tempIndex>2){
            tempIndex=1;
            $(".content-middle ul").css("top","0px")
        }
        $(".content-middle ul").animate({top:tempIndex*-30})
    }
    setInterval(autoPlay,2000);

    $(".content-right").on("mouseenter","li:first-of-type",function(){
        $(this).find("span:eq(1)").css("backgroundPosition","-3px -12px")
        $(this).find(".myOrderList").show();
    }).on("mouseleave","li:first-of-type",function(){
        $(this).find("span:eq(1)").css("backgroundPosition","-3px -16px")
        $(this).find(".myOrderList").hide()
    })
    
    
    $("#all").on("click",function(){
        $("tbody input:checkbox").prop("checked",$(this).prop("checked"))
        $("#delt").prop("checked",$(this).prop("checked"))
    })
    $("#delt").on("click",function(){
        $("tbody input:checkbox").prop("checked",$(this).prop("checked"))
        $("#all").prop("checked",$(this).prop("checked"))
    })

    $("tbody input:checkbox").on("click",function(){
        let cbxCount=$("tbody :checkbox").length;
        let checkedCount=$("tbody :checkbox:checked").length;
        $("#all").prop("checked",cbxCount===checkedCount)
        $("#delt").prop("checked",cbxCount===checkedCount)
    })

    
    $(".addCount").on("click",function(){
        var temp=parseInt($(this).siblings().eq(1).val());
        $(this).siblings().eq(1).val(++temp)
        if(temp>=1){
            $(this).css("color","black")
            $(this).parent("td").find(".cutCount").css("color","black")
        }
    })

    $(".cutCount").on("click",function(){
       var temp=parseInt($(this).siblings().eq(0).val());
        temp=temp<=1?2:temp;
        $(this).siblings().eq(0).val(--temp);
        if(temp==1){
            $(this).css("color","#bdbdbd")
            $(this).parent("td").find(".addCount").css("color","black")
        }
    })


})
