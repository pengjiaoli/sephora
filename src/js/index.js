$(window).load(function(){
    $(".content-middle ul").append('<li><a href="">丝芙兰正品承诺</a></li>');
    var tempIndex=0;
    const autoPlay=()=>{
        tempIndex++;
        if(tempIndex>2){
            tempIndex=1;
            $(".content-middle ul").css("top","0px")
        }
        $(".content-middle ul").animate({top:tempIndex*-30})
        //下面一句可以省略
        $(".content-middle").find("li").eq(1).slideDown("slow");
    }
    setInterval(autoPlay,2000);

    // $(".content-right li:first-of-type").hover(function(){
    //     $(this).find("span:eq(1)").css("backgroundPosition","-3px -12px")
    //     $(this).find(".myOrderList").show();
    // },function(){
    //     $(this).find("span:eq(1)").css("backgroundPosition","-3px -16px")
    //     $(this).find(".myOrderList").hide()
    // })
    $(".content-right").on("mouseenter","li:first-of-type",function(){
        $(this).find("span:eq(1)").css("backgroundPosition","-3px -12px")
        $(this).find(".myOrderList").show();
    }).on("mouseleave","li:first-of-type",function(){
        $(this).find("span:eq(1)").css("backgroundPosition","-3px -16px")
        $(this).find(".myOrderList").hide()
    })

    var temp=0;
    var arr=["新年好礼享不停","红出新势力","新年开运锦囊","迪奥花样星年"]
    setInterval(() => {  
        if(temp>3){
            temp=0
        }
        $(".searchWrap input:text").attr("placeholder",arr[temp++])
    }, 2000);
    
    //全部商品类目事件
    $(".nav-menu").on("mouseenter",function(){
        // $(this).find(".list>li").addClass("")
        $(this).find(".list").on("mouseenter","li",function(e){
            // $(this)是li
            //阻止父元素事件冒泡到子元素,不让谁冒泡就放到谁里面
            $(this).find("li").mouseenter(function(e){
                //二选一
                e.stopPropagation();
                // return false;
            })
                $(this).find("i").show();
                $(e.target).siblings().find("i").hide();
          
            // $(e.target).mouseleave(function(){
            //     $(this).find("i").hide();
            // })
            $(".list>li").addClass("active");
            $(this).siblings().removeClass("active")
         
            var tempIndex=$(this).index()-2;//因为前面有两个i下标
            $(this).parents(".nav-menu").find(".products").eq(tempIndex).addClass("selected").siblings().removeClass("selected")
        })
    }).on("mouseleave",function(){
        $(this).find("i").hide();
        $(this).find("li").removeClass("active");
        $(this).find(".list").siblings().find(".products").removeClass("selected"); 
    })

    //点击全部品牌的显示与隐藏
    $(".menuList").children("li").eq(3).on("mouseenter",function(){
        $(this).find(".muneAll").show();
       
        $(this).find(".Alphabet").on("click","span",function(){
            var alpIndex=$(this).index();
            console.log(alpIndex);
           
            //用一个变量记录当前的不可见区域
            var sTop=$(".muneMain-right").scrollTop();
           $(".muneMain-right").animate({
               scrollTop:$(".muneMain-right .title").eq(alpIndex).position().top+sTop
           })
        })
    }).on("mouseleave",function(){
        $(this).find(".muneAll").hide();
    })

    //轮播图自动轮播
   $(".bannerList>li").each((index)=>{
     $(".bannerImg ol").append("<li></li>")
     if(index==0){
        $(".bannerImg ol").find("li").eq(index).addClass("selt")
     }
   })
  $(".bannerList").append($(".bannerList>li").eq(0).clone(true))
   var imgIndex=0;
   var dianIndex=0;
   const auto=()=>{
        imgIndex++;
        dianIndex++;
        if(imgIndex>$(".bannerList>li").length-1){//因为前面克隆了
            imgIndex=1;//设置为0的话,第一张图会多看2秒的时间
            $(".bannerList").css("left","0px") 
        }
        $(".bannerList").animate({left:-imgIndex*$(".bannerList img").width()})
       if(dianIndex>2){
            dianIndex=0;
       }
        $(".bannerImg ol").find("li").eq(dianIndex).addClass("selt").siblings().removeClass("selt")
   }
   var timer1=setInterval(auto,4000)
   $(".bannerImg").on("mouseenter",function(){
       clearInterval(timer1);
       $(this).find(".bannerArrow").show()
   }).on("mouseleave",function(){
    timer1=setInterval(auto,4000)
    $(this).find(".bannerArrow").hide()
   })

    //点击li切换图片
    $(".bannerImg ol").find("li").on("click",function(e){
        imgIndex=dianIndex=$(e.target).index()-1;//减1是因为auto中imgIndex和dianIndex会加1
        auto()
    })

   //点击箭头轮播
   $(".bRight").on("click",_.debounce(function(){
    auto();
},200))
   $(".bLeft").on("click",_.debounce(function(){
    imgIndex--;
    dianIndex--;
    imgIndex=imgIndex<0?2:imgIndex;
    dianIndex=dianIndex<0?2:dianIndex;
    $(".bannerList").animate({left:-imgIndex*$(".bannerList img").width()})
    $(".bannerImg ol").find("li").eq(dianIndex).addClass("selt").siblings().removeClass("selt")
   },200))

   //小轮播
   $(".smallList>li").each((index)=>{
    $(".smallBanner ol").append("<li></li>")
    if(index==0){
       $(".smallBanner ol").find("li").eq(index).addClass("selt2")
    }
  })
 $(".smallList").append($(".smallList>li").eq(0).clone(true))
  var imgIndex2=0;
  var dianIndex2=0;
  const auto2=()=>{
       imgIndex2++;
       dianIndex2++;
       if(imgIndex2>$(".smallList>li").length-1){//因为前面克隆了
           imgIndex2=1;//设置为0的话,第一张图会多看2秒的时间
           $(".smallList").css("left","0px") 
       }
       $(".smallList").animate({left:-imgIndex2*$(".smallList img").width()})
      if(dianIndex2>1){
           dianIndex2=0;
      }
       $(".smallBanner ol").find("li").eq(dianIndex2).addClass("selt2").siblings().removeClass("selt2")
  }
  var timer2=setInterval(auto2,4000)
  $(".smallBanner").on("mouseenter",function(){
      clearInterval(timer2);
      $(this).find(".brandArrow").show()
  }).on("mouseleave",function(){
   timer2=setInterval(auto2,4000)
   $(this).find(".brandArrow").hide()
  })

   //点击li切换图片
   $(".smallBanner ol").find("li").on("click",function(e){
       imgIndex2=dianIndex2=$(e.target).index()-1;//减1是因为auto中imgIndex和dianIndex会加1
       auto2()
   })

  //点击箭头轮播
  $(".brRight").on("click",_.debounce(function(){
    auto2();
},200))
  $(".brLeft").on("click",_.debounce(function(){
    imgIndex2--;
    dianIndex2--;
    imgIndex2=imgIndex2<0?1:imgIndex2;
    dianIndex2=dianIndex2<0?1:dianIndex2;
    $(".smallList").animate({left:-imgIndex2*$(".smallList img").width()})
    $(".smallBanner ol").find("li").eq(dianIndex2).addClass("selt2").siblings().removeClass("selt2")
   },200))

//品牌的tab切换
$(".tabList").find("li").on("click",function(){
    $(".brandList").eq($(this).index()).removeClass("dis").siblings().addClass("dis")
})



   //窗口滚动事件--吸顶
   $(window).on("scroll",_.debounce(function(){
    var sTop=$(this).scrollTop();
    var clientH=document.documentElement.clientHeight;
    if(sTop>clientH){
        $(".fixTop").fadeIn().css({
            "position":"fixed",
            "top":"0px"
        })
    }else{
     $(".fixTop").fadeOut()
    }
    //右侧吸顶
    if(sTop>$(".brand").offset().top+$(".brandTop").height()){
        $(".fixRight").show().css({
         "position":"fixed",
         "right":"0px",
         "bottom":"60px"
        })
    }else{$(".fixRight").hide()}

    //左侧固定
    if(sTop>$(".pick").offset().top+$(".pick").height()-80){
        $(".fixLeft").show().css({
         "position":"fixed",
         "left":"0px",
         "top":"70px"
        })
    }else{$(".fixLeft").hide()}

    //切换楼层
    
    var index=[...$(".mainContent>.brand")].findIndex(el=>$(el).offset().top>sTop+clientH/2)
    if(index==-1){
        index=$(".mainContent>.brand").length;
    }
    $(".fixLeft li").eq(index-1).addClass("current").siblings().removeClass("current")
}))


      //回到顶部
      $(".fixRight>").find("li").eq(4).on("click",function(){
        $("html,body").animate({scrollTop:0})
    })

      //楼层滚动
      $(".fixLeft li").on("click",function(){
        var sIndex=$(this).index();
        console.log(sIndex);
        var sLength=$(".mainContent").find(".brand").eq(sIndex).offset().top-$(".fixTop").height();
        console.log(sLength);
        $("body,html").animate({
            scrollTop:sLength
        })
    })


})
// $.fn.extend({
//     carousel(){

//     }
// })