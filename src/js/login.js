$(window).load(function(){
    $(".mixLogin ul").on("mouseenter","li",function(){
        $(this).css({
            "backgroundPosition":`-80px ${$(this).index()*-24}px`,
            "cursor":"pointer"
        })
    }).on("mouseleave","li",function(){
        $(this).css({
            "backgroundPosition":`-56px  ${$(this).index()*-24}px`,
        })
    })

    $.validator.addMethod("checkPhone",function(val){
        return /^[1][3-9]\d{9}$/.test(val)
     });
     $.validator.addMethod("checkPwd",function(val){
         console.log(val);
         console.log(/(?:[A-Z])(?:!|#|%)\w{8,16}/g.test(val));
        return/(?:[A-Z])(?:!|#|%)\w{8,16}/g.test(val)
         //密码必须包含一个大写英文字母
         //密码必须包含一个特殊符号(如!, $, #, %)
         //8-16位\w
     })
     $("form").validate({
         rules:{
             //根据input上的name属性名写
             uphone:{
                 required:true,
             },
             upwd:{
                 required:true,
             }
         },
         messages:{
             uphone:{
                 required:"请填写手机号",
             },
             upwd:{
                 required:"请填写密码",
             }
         },
         submitHandler(){
             //ajax默认是异步,则支持promise格式,拿结果可以用then
             $.ajax({
                 url:"./account/login",//省略了 http://127.0.0.1:8080/account/reg
                 type:"post",
                 //前端发送到服务器的数据 data
                 data:$("form").serialize()
             }).then(function(response){//服务器响应到前端的数据data
             //   layer.msg("注册成功")
               if(response.status==200){
                   layer.msg("登录成功");
                   //把登录成功的 用户信息,保存到浏览器的本地存储
                   localStorage.setItem("userinfo",JSON.stringify(response.data));
                   
                   //跳转页面
                   setTimeout(function(){
                       location="index.html"
                   },1000)
               }else{
                   layer.msg("登录失败")
               }
 
             })
             return false;
         }
     })
})