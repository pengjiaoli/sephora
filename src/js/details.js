$(window).load(function(){
    $(".content-middle ul").append('<li><a href="">丝芙兰正品承诺</a></li>');
    var tempIndex=0;
    const autoPlay=()=>{
        tempIndex++;
        if(tempIndex>2){
            tempIndex=1;
            $(".content-middle ul").css("top","0px")
        }
        $(".content-middle ul").animate({top:tempIndex*-30})
        //下面一句可以省略
        $(".content-middle").find("li").eq(1).slideDown("slow");
    }
    setInterval(autoPlay,2000);

    // $(".content-right li:first-of-type").hover(function(){
    //     $(this).find("span:eq(1)").css("backgroundPosition","-3px -12px")
    //     $(this).find(".myOrderList").show();
    // },function(){
    //     $(this).find("span:eq(1)").css("backgroundPosition","-3px -16px")
    //     $(this).find(".myOrderList").hide()
    // })
    $(".content-right").on("mouseenter","li:first-of-type",function(){
        $(this).find("span:eq(1)").css("backgroundPosition","-3px -12px")
        $(this).find(".myOrderList").show();
    }).on("mouseleave","li:first-of-type",function(){
        $(this).find("span:eq(1)").css("backgroundPosition","-3px -16px")
        $(this).find(".myOrderList").hide()
    })

    var temp=0;
    var arr=["新年好礼享不停","红出新势力","新年开运锦囊","迪奥花样星年"]
    setInterval(() => {  
        if(temp>3){
            temp=0
        }
        $(".searchWrap input:text").attr("placeholder",arr[temp++])
    }, 2000);
    
    $(".nav-menu").on("mouseenter",function(){
        // $(this).find(".list>li").addClass("")
        $(this).find(".list").addClass("selected").on("mouseenter","li",function(e){
            // $(this)是li
            //阻止父元素事件冒泡到子元素,不让谁冒泡就放到谁里面
            $(this).find("li").mouseenter(function(e){
                //二选一
                e.stopPropagation();
                 // return false;
            })
            
            $(this).find("i").show();
            $(e.target).siblings().find("i").hide();

            //写的比较繁琐了
            $(".list>li").addClass("active")
            $(this).siblings().removeClass("active")
         
            var tempIndex=$(this).index()-2;//因为前面有两个i下标
            $(this).parents(".nav-menu").find(".products").eq(tempIndex).addClass("selected").siblings().removeClass("selected")
        })
    }).on("mouseleave",function(){
        $(this).find(".list").removeClass("selected").siblings().find(".products").removeClass("selected"); 
    })

    //点击全部品牌的显示与隐藏
    $(".menuList").children("li").eq(3).on("mouseenter",function(){
        $(this).find(".muneAll").show();
       
        $(this).find(".Alphabet").on("click","span",function(){
            var alpIndex=$(this).index();
            console.log(alpIndex);
           
            //用一个变量记录当前的不可见区域
            var sTop=$(".muneMain-right").scrollTop();
           $(".muneMain-right").animate({
               scrollTop:$(".muneMain-right .title").eq(alpIndex).position().top+sTop
           })
        })
    }).on("mouseleave",function(){
        $(this).find(".muneAll").hide();
    })
    //放大镜
    	/*
		 公式:小图/大图=小区域/大区域
		     只能修改小区域
		     小区域=(小图/大图)*大区域
		     比例=大区域/小区域
		 */
    $(".smallImg").on("mouseenter",function(){
        $(".smallArea").show();
        $(".bigArea").show();
        $(".smallArea").css({
            width:$(".smallImg").width()/$(".bigImg img").width()*$(".bigArea").width(),
            height:$(".smallImg").height()/$(".bigImg img").height()*$(".bigArea").height()
        })
        $(this).on("mousemove",function(e){
            let mx=e.pageX-$(this).offset().left-$(".smallArea").width()/2;
            let my=e.pageY-$(this).offset().top-$(".smallArea").height()/2;
            if(mx<=0){
                mx=0
            }
            if(mx>$(this).width()-$(".smallArea").width()){
                mx=$(this).width()-$(".smallArea").width()
            }
            if(my<=0){
                my=0;
            }
            if(my>$(this).height()-$(".smallArea").height()){
                my=$(this).height()-$(".smallArea").height()
            }

            //比例
            let oScale=$(".bigArea").width()/$(".smallArea").width();

            $(".bigImg img").css({
                left:oScale*mx*-1,
                top:oScale*my*-1
            })
            $(".smallArea").css({
                left:mx,
                top:my
            })
        })
    }).on("mouseleave",function(){
        $(".smallArea").hide();
        $(".bigArea").hide();
    })
    //点击箭头换图
    $(".arrowLeft").on("click",function(){
        $(this).css("cursor","pointer")
        $(".smallImg img").attr("src","./image/detailsImage/1_n_09954_640x640.jpg")
        $(".bigImg img").attr("src","./image/detailsImage/1_n_09954_1200x1200.jpg")
    })
    $(".arrowRight").on("click",function(){
        $(this).css("cursor","pointer")
        $(".smallImg img").attr("src","./image/detailsImage/2_n_09954_640x640.jpg")
        $(".bigImg img").attr("src","./image/detailsImage/2_n_09954_1200x1200.jpg")
    })

    //点击图片换路径
    $(".controlModule").find("img").click(function(){
        //每次点击的是li,li里面只有一个img,如果直接$(this).index(),那么下标永远是0
        //所以应该获取父级的下标
        var tempIndex=$(this).parent("li").index()+1;
        $(".smallImg img").attr("src",`./image/detailsImage/${tempIndex}_n_09954_1200x1200.jpg`)
    })

 
//促销详情的事件
    var flag=true;
    $(".activity i").on("click",function(){
        var tempIndex=$(this).parents("li").index();
        if(flag){
            $(this).css("backgroundPosition","-50px -18px")
            $(".promotion").eq(tempIndex).show()
            flag=false;
        }else{
            $(this).css("backgroundPosition","-40px -18px")
            $(".promotion").eq(tempIndex).hide()
            flag=true
        }
        
    })

    //点击套装换图
    $(".boxList").on("click","li",function(){
        $(this).addClass("current").siblings().removeClass("current")
        if($(this).index()>0){
            $(".smallImg img").attr("src","./image/detailsImage/1_n_09998_640x640.jpg")
            $(".bigImg img").attr("src","./image/detailsImage/1_n_09998_1200x1200.jpg")
        }else{
            $(".smallImg img").attr("src","./image/detailsImage/1_n_09954_640x640.jpg")
            $(".bigImg img").attr("src","./image/detailsImage/1_n_09954_1200x1200.jpg")
        }
    })

    //点击箭头更改数量
    $(".numberArrow").on("click",".topA",function(){
        var temp=parseInt($(this).parents(".number").find("input:text").val());
        if($(this).index()==0){
            // temp=temp>9?9:temp;
            if(temp<10)
            $(this).parents(".number").find("input:text").val(++temp)
        }else{
            // temp=temp<=1?2:temp;
            if(temp>1)
            $(this).parents(".number").find("input:text").val(--temp);
        }
        console.log(temp);
        if(temp>1&&temp<10){
            $(".topArrow").css("backgroundPosition","-50px -18px")
            $(".bottomArrow").css("backgroundPosition","-40px -18px")
        }else if(temp==10){
            $(".topArrow").css("backgroundPosition","-50px -42px")
            $(".bottomArrow").css("backgroundPosition","-40px -18px")
        }else{
            $(".topArrow").css("backgroundPosition","-50px -18px")
            $(".bottomArrow").css("backgroundPosition","-40px -42px")
        } 
    })



})