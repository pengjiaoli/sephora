const { SSL_OP_ALLOW_UNSAFE_LEGACY_RENEGOTIATION } = require("constants");

$(window).load(function(){
    $.validator.addMethod("checkPhone",function(val){
       return /^[1][3-9]\d{9}$/.test(val)
    });
    $.validator.addMethod("checkPwd",function(val){
        console.log(val);
        console.log(/(?:[A-Z])(?:!|#|%)\w{8,16}/g.test(val));
       return/(?:[A-Z])(?:!|#|%)\w{8,16}/g.test(val)
        //密码必须包含一个大写英文字母
        //密码必须包含一个特殊符号(如!, $, #, %)
        //8-16位\w
    })
    $("form").validate({
        rules:{
            //根据input上的name属性名写
            uphone:{
                required:true,
                checkPhone:true
            },
            upwd:{
                required:true,
                checkPwd:true
            },
            upwd2:{
                equalTo:'#upwd',
            }

        },
        messages:{
            uphone:{
                required:"请填写手机号",
                checkPhone:"手机号不合法"
            },
            upwd:{
                required:"密码不能少于8位",
                checkPwd:"密码输入不合法"
            },
            upwd2:{
                equalTo:"两次密码输入不一致"
            }
        },
        submitHandler(){
            //ajax默认是异步,则支持promise格式,拿结果可以用then
            $.ajax({
                url:"./account/reg",//省略了 http://127.0.0.1:8080/account/reg
                type:"post",
                //前端发送到服务器的数据 data
                data:$("form").serialize()
            }).then(function(response){//服务器响应到前端的数据data
            //   layer.msg("注册成功")
              if(response.status==200){
                  layer.msg("注册成功");

                  setTimeout(function(){
                    location="login.html"
                },1000)

              }else{
                  layer.msg("注册失败")
              }

            })
            return false;
        }
    })
})