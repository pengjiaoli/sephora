const mysql=require("mysql");

let pool=mysql.createPool({
    host:'localhost',
    user:'root',
    password:'root',
    database:'sephora',
    port:3306,
    connectionLimit:10
});

const exec=(sql,params)=>{
    return new Promise((resolve,reject)=>{
        pool.getConnection((err,conn)=>{
            if(err){
                console.log("数据库连接失败:"+err);
                reject(err)
            }
            conn.query(sql,params,(err,result)=>{
                if(err){
                    console.log("SQL语句执行失败:"+err);
                    reject(err);
                }
                resolve(result);
                conn.release();//释放连接
            })
        })
    })
}
module.exports={
    exec
}

