//引入模块
const express=require("express");
const bodyParser=require("body-parser");

//创建服务
let server=express();
//静态资源
server.use(express.static("./../src"))


//中间件
server.use(bodyParser.urlencoded({extended:false}));
server.use(bodyParser.json());
//配置路由 (保安大爷上岗)   一个js文件就是一个模块
//根据不同的路径,访问服务器上不同的资源
server.use("/account",require("./router/accountRouter").router);
//购物车路由
server.use("/cart",require("./router/cartRouter").router)

//设置监听
server.listen(8080,()=>{
    console.log("服务启动完毕!");
})