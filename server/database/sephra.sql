/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.26 : Database - sephora
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sephora` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `sephora`;

/*Table structure for table `cart` */

DROP TABLE IF EXISTS `cart`;

CREATE TABLE `cart` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `c_msg` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_price` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_num` int(11) DEFAULT NULL,
  `c_total` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_img` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_createtime` datetime DEFAULT NULL,
  `c_status` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `c_desc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`c_id`),
  KEY `u_id` (`u_id`),
  CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`u_id`) REFERENCES `userinfo` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `cart` */

/*Table structure for table `userinfo` */

DROP TABLE IF EXISTS `userinfo`;

CREATE TABLE `userinfo` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_phone` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_pwd` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `u_createtime` datetime DEFAULT NULL,
  `u_status` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `u_desc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `u_phone` (`u_phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `userinfo` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
