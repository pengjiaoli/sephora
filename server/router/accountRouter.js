//用户管理的 路由
//1.引入 express框架
const express=require("express");//第2次加载模块的缓存
const db=require("../modules/DBhelper");
const moment=require("moment");
const { request, response } = require("express");

//2.创建路由(聘请保安大爷)
let router=express.Router();

//登录接口
router.post("/login",async function(){
    let userObj=request.body;
    let sql="SELECT*FROM `userinfo` WHERE `u_phone`=? AND `u_pwd`=?;";
    let params=[
        //后台接口的字段
        userObj.uphone,
        userObj.upwd
    ];
    let result=await db.exec(sql,params);
    let isLogin=result && result.length >=1;
    if(isLogin){
        //查询返回的是一个数组 [{}]
      delete(result[0].u_pwd);//是u_pwd,这个字段来自于数据库
    }
    let messages={
        msg:isLogin?"登录成功":"登录失败",
        data:isLogin?result:'',
        status:isLogin?200:-200
    }
    response.json(messages);

})
//注册的接口
router.post("/reg",async function(request,response,next){
    //1.得到前端传过来的数据
    let userObj=request.body;
    //2.sql语句
    let sql="INSERT INTO `userinfo` (`u_phone`,`u_pwd`,`u_createtime`)VALUES(?,?,?);";
    let params=[
        ////后台接口的字段
        userObj.uphone,
        userObj.upwd,
        moment().format("YYYY-MM-DD HH:mm:ss")
    ];
    let result=await db.exec(sql,params);
    //注册返回的是对象
    let isReg=result&&result.affectedRows;
    let messages={
        msg:isReg?"注册成功":"注册失败",
        status:isReg?200:-200
    }
    response.json(messages)
})

//暴露
module.exports={
    router
}

//http://127.0.0.1:8080/account/login
//http://127.0.0.1:8080/account/reg